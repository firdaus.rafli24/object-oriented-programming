public class ArrayMulti{
	
	public static void main(String [] args){
	
	//	Elemen 512 x 128 dari integer array
	
		int [][] twoD = new int[512][128];

	//	Karakter array 8 x 16 x 24
	
		char [][][] threeD = new char[6][16][24];
		
	//	String array 4 baris x 2 kolom

		String [][] kiddos = {{"Albarra","Aldebaran"},
				{"Arham","Adit"},
				{"Koko","Zyo"},
				{"Kalista","Zaki"}
				};
	
		System.out.println(kiddos[0][0]);
	}
}