import OOP.Lion;
import OOP.Horse;
import OOP.Kangaroo;
import java.lang.*;
import java.awt.Color;
import java.util.Scanner;

public class Zoo{

	public static void main(String [] args){
	
		Horse[] Kuda= new Horse[5];
		Lion[] Singa = new Lion[5];
		Kangaroo[] Kangguru = new Kangaroo[5];
		Scanner dataIn = new Scanner(System.in);
		Scanner dataSt = new Scanner(System.in);
		
	//	Horse	
		for(int i=0; i<Kuda.length;i++){
			
			Kuda[i] = new Horse();
			System.out.print("Masukan Nama Kuda: ");
			Kuda[i].nama = dataSt.nextLine();
			System.out.print("Masukan Usia Kuda: ");
			Kuda[i].usia = dataIn.nextInt();
			System.out.print("Masukan Majikan Kuda: ");
			Kuda[i].majikan = dataSt.nextLine();
			Kuda[i].diadopsi();
			Kuda[i].cetakInformasi();
			System.out.println();			
		}
	//	Lion
		for(int i=0; i<Singa.length;i++){
			
			Singa[i] = new Lion();
			System.out.print("Masukan Nama Singa: ");
			Singa[i].nama = dataSt.nextLine();
			System.out.print("Masukan Usia Singa");
			Singa[i].usia = dataIn.nextInt();
			System.out.print("Masukan Majikan Singa");
			Singa[i].majikan = dataSt.nextLine();
			Singa[i].diadopsi();
			Singa[i].cetakInformasi();
			System.out.println();			
		}
	//	Kangaroo
		for(int i=0; i<Kangguru.length;i++){
			
			Kangguru[i] = new Kangaroo();
			System.out.print("Masukan Nama Kangguru");
			Kangguru[i].nama = dataSt.nextLine();
			System.out.print("Masukan Usia Kangguru");
			Kangguru[i].usia = dataIn.nextInt();
			System.out.print("Masukan Majikan Kangguru");
			Kangguru[i].majikan = dataSt.nextLine();
			Kangguru[i].diadopsi();
			Kangguru[i].cetakInformasi();
			System.out.println();			
		}	
	}	
}
		