import java.util.Scanner;
public class Matrix{
	
	public static void main(String [] args){
	
		int baris,kolom;
		Scanner dataIn = new Scanner(System.in);
	
	
		System.out.println("Masukan jumlah baris: ");
		baris = dataIn.nextInt();	
		System.out.println("Masukan jumlah kolom");
		kolom = dataIn.nextInt();
		int[][] Matriks = new int[baris][kolom];
		System.out.println();
		for(int i= 0; i< baris;i++){
			
			for(int j = 0; j< kolom; j++){
	
				Matriks[i][j] = i + j;
			}
		}
		
		for(int i = 0;i<baris;i++){
			
			for(int j = 0; j<kolom;j++){
			
				System.out.print(Matriks [i][j] + "  ");
			}
			System.out.println();
		}				
	}
}