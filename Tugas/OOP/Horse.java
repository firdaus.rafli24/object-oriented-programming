package OOP;
import java.lang.*;
import java.awt.Color;

public class Horse{

	public String nama;
	public int usia;
	public boolean statusJinak;
	public double beratBadan;
	public String majikan;

	public void cetakInformasi(){
	
		System.out.println("Kuda bernama: " +nama);
		System.out.println("Usia : " +usia);
		System.out.println("Diadopsi oleh: " +majikan);
		System.out.println("Jinak ?: " +statusJinak);
		
	}

	public void diadopsi(){
		
		String m;
		m = majikan;
		statusJinak = true;
	}

	public boolean apakahJinak(){

		return(statusJinak);
	}

}


	