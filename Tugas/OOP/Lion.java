package OOP;
import java.lang.*;
import java.awt.Color;

public class Lion{
	public String nama;
	public int usia;
	public double beratBadan;
	public boolean statusJinak;
	public String majikan;
	
	public void cetakInformasi(){
	
		System.out.println("Singa Bernama: " +nama);
		System.out.println("Berat Badan: " +beratBadan);
		System.out.println("Jinak ?: "  +apakahJinak());
		System.out.println("Diadopsi oleh: " +majikan);
	}
	
	public void diadopsi(){
	
		String m;
		m = majikan;
		statusJinak = true;
	}
	
	public boolean apakahJinak(){
	
		return(statusJinak);
	}
	
}