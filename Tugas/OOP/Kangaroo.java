package OOP;
import java.lang.*;
import java.awt.Color;

public class Kangaroo{

	public String nama;
	public Color warna;
	public int usia;
	public boolean statusJinak;
	public double beratBadan;
	public String majikan;
	
	
	public void cetakInformasi(){

		System.out.println("Kangaroo bernama: " +nama);
		System.out.println("Usia : " +usia);
		System.out.println("Jinak ?: " +statusJinak);
		System.out.println("Diadopsi oleh: " +majikan);
	
	}

	public void diadopsi(){
	
		String m;
		m = majikan;
		statusJinak = true;
	
	}

	public boolean apakahJinak(){
		
		return(statusJinak);
	}
}