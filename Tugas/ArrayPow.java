import java.lang.Math;

public class ArrayPow{
	
	public static void main(String [] args){
		
		int [] anArray = new int[11];

		for(int i=0;i<anArray.length;i++){
			
			double N;
			N = Math.pow(2,i);
			anArray[i] = (int) Math.round(N);
			System.out.print(anArray[i] +" ");
		}
	}
}
			

